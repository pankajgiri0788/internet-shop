package common;

public enum CreditCardType {
    GOLD(20),
    SILVER(10),
    NORMAL(0);

    private Integer discount;

    CreditCardType(Integer discount) {
        this.discount = discount;
    }

    public Integer getDiscount() {
        return discount;
    }
}
