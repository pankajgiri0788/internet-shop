package service.impl;

import model.Product;
import service.ShoppingCartService;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCartServiceImpl implements ShoppingCartService {

    // dummy datasource
    List<Product> productDatasource = new ArrayList<>();

    @Override
    public Product addProduct(Product product) {
        productDatasource.add(product);
        return product;
    }

    @Override
    public void removeProduct(Integer productId) {
        productDatasource.removeIf((prd) -> prd.getId().equals(productId));
    }

    @Override
    public List<Product> getProducts() {
        return productDatasource;
    }
}
