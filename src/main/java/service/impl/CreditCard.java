package service.impl;

import common.CreditCardType;
import model.Product;
import service.PaymentService;

import java.util.List;
import java.util.Objects;

public class CreditCard implements PaymentService {

    private final CreditCardType cardType;

    public CreditCard(CreditCardType cardType) {
        this.cardType = cardType;
    }

    @Override
    public double calculateProductsPrice(List<Product> products) {
        double totalPrice = 0;
        if (Objects.isNull(products)) {
            return totalPrice;
        }
        return products.stream()
                .map(Product::getPrice)
                .reduce(totalPrice, Double::sum);
    }

    private double applyDiscount(double totalPrice) {
        if (!cardType.equals(CreditCardType.NORMAL)) {
            return (totalPrice * cardType.getDiscount()) * 100;
        }
        return totalPrice;
    }
}
