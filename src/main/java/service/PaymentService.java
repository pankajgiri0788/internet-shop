package service;

import model.Product;

import java.util.List;

public interface PaymentService {

    double calculateProductsPrice(List<Product> products);
}
