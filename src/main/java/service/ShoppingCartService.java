package service;

import model.Product;

import java.util.List;

public interface ShoppingCartService {

    Product addProduct(Product product);

    void removeProduct(Integer productId);

    List<Product> getProducts();
}
