package resource;

import common.CreditCardType;
import model.Product;
import service.PaymentService;
import service.ShoppingCartService;
import service.impl.CreditCard;
import service.impl.ShoppingCartServiceImpl;

public class InternetStore {

    // If used dependency injection then auto-wire this
    private final PaymentService paymentService;
    private final ShoppingCartService cartService;

    public InternetStore(PaymentService paymentService, ShoppingCartService cartService) {
        this.paymentService = paymentService;
        this.cartService = cartService;
    }

    public static void main(String[] args) {
        InternetStore store = new InternetStore(
                new CreditCard(CreditCardType.GOLD),
                new ShoppingCartServiceImpl()
        );
        // Add and remove the products
        store.cartService.addProduct(new Product(1, "Prd1", 100.5));
        store.cartService.addProduct(new Product(2, "Prd2", 119.5));
        store.cartService.addProduct(new Product(3, "Prd3", 130.0));
        Double totalPriceAfterDiscount = store.paymentService.calculateProductsPrice(store.cartService.getProducts());
        System.out.println("Total price of products" + totalPriceAfterDiscount);
    }
}
